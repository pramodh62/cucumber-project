@demo
Feature: Common Banking Transactions

  Scenario: Cash withdrawal below Overdraft Limit
    Given User is on Home Page
      And Logs in as an existing Customer
    When He withdraws an amount less than the overdraft limit
    Then Transaction is Successful

  Scenario: Successful Cash Deposit transaction
    Given User is on Home Page
      And Logs in as an existing Customer
    When He Deposits a valid amount of Money
    Then Deposit is Successful
