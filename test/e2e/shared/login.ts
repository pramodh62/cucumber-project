import { IdentificationType, CommonTasks } from './common-tasks';

// Component Locators
export const LocatorsLogin = {

    customerLoginButton: {
        type: IdentificationType[IdentificationType.Xpath],
        value: './/button[contains(text(), "Customer Login")]'
    },

    customerNameList: {
        type: IdentificationType[IdentificationType.Id],
        value: 'userSelect'
    },

    loginButton: {
        type: IdentificationType[IdentificationType.ButtonText],
        value: 'Login'
    }
}

export class LoginPage extends CommonTasks {

    // Customer Login Button
    customerLoginButton = this.elementLocator(LocatorsLogin.customerLoginButton);

    // Customer Name List
    customerNameList = this.elementLocator(LocatorsLogin.customerNameList);

    // Login Button
    loginButton = this.elementLocator(LocatorsLogin.loginButton);

}
