import { ContractInformationComponent } from './../contract-information/contract-information.component';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { StepperService, CONTRACT_TYPES, STEPPER_LABEL, IMAGE_PATH, STEPPER_URL } from './../../shared';
import { StepModel } from './step.model';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, OnDestroy {
  private: ContractInformationComponent;
  public stepper: Array<StepModel> = [];
  public contractType: string;
  public step1; step2; step3; step4; step5;
  private _subscription: Subscription;
  public currentStep: number;
  public imageDir = IMAGE_PATH;

  constructor(
    private _stepperService: StepperService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeStepper();
    this.detectContractTypeChanges();
    this._subscription = this._stepperService.componentChange$.subscribe(
      stepNumber => {
        this.updateStepper(stepNumber);
        this.cdr.detectChanges();
      }
    );
  }

  initializeStepper() {
    this.step1 = this.stepper.push(new StepModel(1, STEPPER_LABEL.PRICING_INFORMATION, STEPPER_URL.PRICING_INFORMATION_URL));
    this.step2 = this.stepper.push(new StepModel(2, STEPPER_LABEL.DISTRIBUTION_CENTERS, STEPPER_URL.DISTRIBUTION_CENTERS_URL));
    this.step3 = this.stepper.push(new StepModel(3, STEPPER_LABEL.MARKUP, STEPPER_URL.MARKUP_URL));
    this.step4 = this.stepper.push(new StepModel(4, STEPPER_LABEL.SPLIT_CASE, STEPPER_URL.SPLIT_CASE_URL));
    this.step5 = this.stepper.push(new StepModel(5, STEPPER_LABEL.REVIEW, STEPPER_URL.REVIEW_URL));
  }

  detectContractTypeChanges() {
    this._stepperService.contractTypeChange$.subscribe(selectedContractType => {
      this.contractType = selectedContractType;
      this.showHideSteps();
      // this.cdr.detectChanges();
    });
  }

  showHideSteps() {
    if (this.contractType === CONTRACT_TYPES.IFS) {
      if (this.stepper.length > 3) {
        setTimeout(() => {
          this.stepper.splice(1, 1);
          this.stepper.splice(2, 1);
        });
      }
    }
  }

  updateStepper(stepNumber: number) {
    this.currentStep = stepNumber;
    this.stepper.forEach(step => {
      if (step.stepNumber < this.currentStep) {
        step.isCurrent = false;
        step.isDisabled = false;
        step.isCompleted = true;
      } else if (step.stepNumber > this.currentStep) {
        step.isCurrent = false;
        step.isDisabled = true;
        step.isCompleted = false;
      } else if (step.stepNumber === this.currentStep) {
        step.isCurrent = true;
        step.isDisabled = false;
        step.isCompleted = false;
      }
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this._subscription.unsubscribe();
  }
}
