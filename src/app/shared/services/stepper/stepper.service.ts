import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class StepperService {
  // Observable string sources
  private contractTypeChangeSource = new Subject<string>();
  private componentChangeSource = new Subject<any>();

  // Observable string streams
  contractTypeChange$ = this.contractTypeChangeSource.asObservable();
  componentChange$ = this.componentChangeSource.asObservable();

  contractTypeChange(contractType: string) {
    this.contractTypeChangeSource.next(contractType);
  }

  // current component loaded
  currentStep(stepNumber: number) {
    this.componentChangeSource.next(stepNumber);
  }

  getCurrentStep() {
    return this.componentChangeSource;
  }
}
