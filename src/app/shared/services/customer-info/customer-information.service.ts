import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpParams } from '@angular/common/http';

import { apiUrls } from './../app.url';
import { CustomerInformation } from './../../../contract';

@Injectable()
export class CustomerInformationService {

  private savePricingInfoURL = apiUrls.savePricingInformationURL;
  private fetchCPPSequenceURL = apiUrls.fetchCPPSequenceURL;

    constructor(private http: HttpClient) {}

      public savePricingInformation(customerInformation: CustomerInformation): Observable<CustomerInformation> {
        return this.http
        .post(this.savePricingInfoURL, customerInformation)
        .map((res: CustomerInformation) => res);
      }

      public fetchCPPSequenceId(cppId: string): Observable<CustomerInformation> {
        const params = new HttpParams().set('contractPriceProfileId', cppId);
        return this.http
        .get(this.fetchCPPSequenceURL, {params})
        .map((res: CustomerInformation) => res);
      }
}
